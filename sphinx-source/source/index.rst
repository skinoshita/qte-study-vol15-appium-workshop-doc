============================================
Appiumで体験するモバイルアプリのテスト自動化
============================================

はじめに 
========

今回のワークショップを通じて Appium がどういうものかを知り、実際のテスト業務で活用できるかを踏まえて体験してみましょう。

最初に Appium の動作環境を整えましょう。

動作環境を整えたら、GitHubにコミットされているサンプルソースを動かしてみましょう。

なお、使用するサンプルコードは Java です。

また、対象とするプラットフォームは Android です。

必要なもの
==========

* PC (Windows, Mac)
* テキストエディタ
* Androidエミュレータ
* Java Development Kit
* Maven

事前準備
========

1. Appiumのインストール
-----------------------
|

1.1. Appium の `公式サイト <http://appium.io/?lang=en>`_ を表示し、「Download Appium」をクリックします。
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/qte-study-vol15-img01.png

|

・Windowsの場合、「AppiumForWindows-1.3.4.1.zip」をダウンロードしてください。

・Macの場合、「appium-1.3.7.dmg」をダウンロードしてください。

.. image:: images/qte-study-vol15-img02.png

1.2. ダウンロードしたzipファイルもしくはdmgファイルを使って、Appiumをインストールします。
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


|

1.2.1. Windowsの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

・AppiumForWindows-1.3.4.1.zipを解凍します。解凍したフォルダをCドライブの直下に移動します。

.. image:: images/qte-study-vol15-appium-win-inst-01.png

|

1.2.2. Macの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

・appium-1.3.7.dmgをダブルクリックします。

|

・AppiumのアプリケーションをApplicationsフォルダにコピーします。

.. image:: images/qte-study-vol15-appium-mac-inst-01.png

|

2. Java Development Kit のインストール
------------------------------------------

|

2.1. Oracle の `公式サイト <http://www.oracle.com/technetwork/java/javase/downloads/index.html>`_ を表示し、JDKの「Download」をクリックします。
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/qte-study-vol15-jdk-cmn-inst-01.png

|

2.2. 「Accept Licence Agreement」を選択します。
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/qte-study-vol15-jdk-cmn-inst-02.png

|

* Windows 64bitの場合、「Windows x64」のリンクをクリックします。
* Windwos 32bitの場合、「Windows x86」のリンクをクリックします。
* Macの場合、「Mac OS X x64」のリンクをクリックします。

|

2.3. Java Development Kitをインストールします。
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


2.3.1. Windowsの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

・ダウンロードしたインストーラのファイルをダブルクリックします。

・「次(N)>」をクリックします。

.. image:: images/qte-study-vol15-jdk-win-inst-01.png

|

・「次(N)>」をクリックします。

.. image:: images/qte-study-vol15-jdk-win-inst-02.png

|

・「次(N)>」をクリックします。

.. image:: images/qte-study-vol15-jdk-win-inst-03.png

|

・「閉じる(C)>」をクリックします。

.. image:: images/qte-study-vol15-jdk-win-inst-04.png

|

・環境変数：JAVA_HOMEを設定します。変数値は先程インストールしたJava Development Kitのパスになります。

.. image:: images/qte-study-vol15-jdk-win-inst-05.png

|

2.3.2. Macの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

・ダウンロードしたインストーラのファイルをダブルクリックします。

.. image:: images/qte-study-vol15-jdk-mac-inst-01.png

|

・インストールダイアログが表示されるので、アイコンをクリックします。

.. image:: images/qte-study-vol15-jdk-mac-inst-02.png

|

・「続ける」をクリックします。

.. image:: images/qte-study-vol15-jdk-mac-inst-03.png

|

・「インストール」をクリックします。

.. image:: images/qte-study-vol15-jdk-mac-inst-04.png

|

・パスワードを入力して「ソフトウェアをインストール」をクリックします。

.. image:: images/qte-study-vol15-jdk-mac-inst-05.png

|

・「閉じる」をクリックします。

.. image:: images/qte-study-vol15-jdk-mac-inst-06.png

|

3. Androidエミュレータのインストール
-------------------------------------
|

3.1. Androidエミュレータをインストールするために、Android SDKをインストールします。
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

3.1.1. Windowsの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

・Android Developer の `公式サイト <https://developer.android.com/sdk/index.html>`_ を表示し、「SDK Tools Only」内のWindowsのexeファイルをダウンロードします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-01.png

|

・利用規約に同意して、ダウンロードボタンをクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-02.png

|

・ダウンロードしたインストーラをダブルクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-03.png

|

・「Next >」をクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-04.png

|

・「Next >」をクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-05.png

|

・「Next >」をクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-06.png

|

・「Next >」をクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-07.png

|

・「Install」をクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-08.png

|

・「Next >」をクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-09.png

|

・「Finish」をクリックします。

.. image:: images/qte-study-vol15-android-sdk-win-inst-10.png

|

・環境変数：ANDROID_HOMEを設定します。変数値は先程インストールしたAndroid SDKのパスになります。

.. image:: images/qte-study-vol15-android-sdk-win-inst-11.png

|

3.1.2. Macの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

・Homebrewを使ってインストールします。Homebrewのインストールがわからない方は `こちら <http://brew.sh/index_ja.html>`_ 。

|

・Homebrewのインストールが完了したら、ターミナルを起動します。「brew install android-sdk」と入力し、Enterを押します。

.. image:: images/qte-study-vol15-android-sdk-mac-inst-01.png

|

・viエディタで~/.bash_profileを開きます。

.. image:: images/qte-study-vol15-android-sdk-mac-inst-02.png

|

・~/.bash_profileに環境変数：ANDROID_HOMEの設定を追加します。

.. image:: images/qte-study-vol15-android-sdk-mac-inst-03.png

|

・ターミナルで「source ~/.bash_profile」と入力し、Enterを押します。

.. image:: images/qte-study-vol15-android-sdk-mac-inst-04.png

|

・ターミナルで「printenv」と入力し、Enterを押します。環境変数：ANDROID_HOMEが設定されていることを確認します。

.. image:: images/qte-study-vol15-android-sdk-mac-inst-05.png

|

4. Mavenのインストール
-------------------------------------

4.1.1. Windowsの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

・Apache Maven Project の `公式サイト <http://maven.apache.org/>`_ を表示し、「Get Maven」の「Download」をクリックします。

.. image:: images/qte-study-vol15-maven-win-inst-01.png

|

・「apache-maven-3.3.1-bin.zip」をクリックします。

.. image:: images/qte-study-vol15-maven-win-inst-02.png

|

・ダウンロードした「apache-maven-3.3.1-bin.zip」を解凍します。

.. image:: images/qte-study-vol15-maven-win-inst-03.png

|

・解凍してできたフォルダをCドライブの直下にコピーします。

.. image:: images/qte-study-vol15-maven-win-inst-04.png

|

・環境変数のパスに「C:\apache-maven-3.3.1\bin」を追加します。

.. image:: images/qte-study-vol15-maven-win-inst-05.png

|

・コマンドプロンプトを開き、「mvn --version」を実行します。Mavenのバージョンが表示されたらインストール完了です。

.. image:: images/qte-study-vol15-maven-win-inst-06.png

|

4.1.2. Macの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

・ターミナルを起動します。ターミナルで「brew install maven」と入力し、Enterを押します。

.. image:: images/qte-study-vol15-maven-mac-inst-01.png

|

・ターミナルで「mvn --version」を実行します。Mavenのバージョンが表示されたらインストール完了です。

.. image:: images/qte-study-vol15-maven-mac-inst-02.png

|

レッスン0：Appiumのサンプルコードをダウンロードしよう
============================================================

|

では、まず初めにサンプルコードを `GitHub <https://github.com/appium/sample-code>`_ からダウンロードしましょう。

`GitHub <https://github.com/appium/sample-code>`_ のサイトの右下にある「Download ZIP」をクリックします。

.. image:: images/qte-study-vol15-lesson0-01.png

|

レッスン1：Android - WebViewアプリをAppiumで動かしてみよう
============================================================

|

サンプルコードのダウンロードが完了したら、Android上で動くWebViewアプリをAppiumで動かしてみましょう。

|

Windowsの場合
----------------------------------------------

|

(1) Appiumの起動
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

・「1.2.1. Windowsの場合」でCドライブの直下にコピーしたフォルダから「Appium.exe」をダブルクリックします。

・Appiumの起動後、「▶」をクリックします。

.. image:: images/qte-study-vol15-lesson1-win-01.png

|

(2) Android エミュレーターの起動
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

・Windowsの「スタート」-「すべてのプログラム」-「Android SDK Tools」-「SDK Manager」を起動します。

・「Android SDK Manager」で「Android 4.4.2 (API 19)」にチェックを入れて、インストールボタンをクリックします。

.. image:: images/qte-study-vol15-lesson1-win-02.png

|

・「Accept License」にチェックを入れて、「install」をクリックします。

.. image:: images/qte-study-vol15-lesson1-win-03.png

|

・「スタート」 - 「すべてのプログラム」 - 「Android SDK Tools」 - 「AVD Manager」をクリックします。

・「Android Virtual Device (AVD) Manager」が起動するので「Create」をクリックします。

.. image:: images/qte-study-vol15-lesson1-win-04.png

|

・下記の図のとおりに入力して、「OK」をクリックします。

.. image:: images/qte-study-vol15-lesson1-win-05.png

|

・完了ダイアログが表示されるので、「OK」をクリックします。

.. image:: images/qte-study-vol15-lesson1-win-06.png

|

・「Android Virtual Device (AVD) Manager」で先ほど作成した仮想デバイスを選択して、「Start...」をクリックします。

.. image:: images/qte-study-vol15-lesson1-win-07.png

|

・「Launch」をクリックします。

.. image:: images/qte-study-vol15-lesson1-win-08.png

|

・Android エミュレーターが起動することを確認します。

.. image:: images/qte-study-vol15-lesson1-win-09.png

|

(3) テストコードの実行
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

・sample-code 配下の examples/java/junit/pom.xml をテキストエディタで開きます。以下の通りに修正し、保存します。

.. image:: images/qte-study-vol15-lesson1-win-10.png

|

・コマンドプロンプトを起動します。コマンドプロンプト起動後、「レッスン0」でダウンロードした sample-code 配下の examples/java/junit まで移動します。

.. image:: images/qte-study-vol15-lesson1-win-11.png

|

・コマンドプロンプトで「mvn -Dtest=com.saucelabs.appium.AndroidWebViewTest test」と入力し、Enterを押します。

.. image:: images/qte-study-vol15-lesson1-win-12.png

|

Macの場合
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|

(1) Appiumの起動
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

・Applicationsフォルダから「Appium」をダブルクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-01.png

|

・Appiumの起動後、Androidのアイコンを選択し、「Launch」をクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-02.png

|

(2) Android エミュレーターの起動
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

・ターミナルを起動します。ターミナル起動後、「android」と入力し、Enterを押します。

.. image:: images/qte-study-vol15-lesson1-mac-03.png

|

・「Android SDK Manager」で「Android 4.4.2 (API 19)」にチェックを入れて、インストールボタンをクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-04.png

|

・「Accept License」にチェックを入れて、「install」をクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-05.png

|

・画面上部のメニューから「Tools」-「Manage AVDs..」をクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-06.png

|

・「Android Virtual Device (AVD) Manager」が起動するので「Create」をクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-07.png

|

・下記の図のとおりに入力して、「OK」をクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-08.png

|

・完了ダイアログが表示されるので、「OK」をクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-09.png

|

・「Android Virtual Device (AVD) Manager」で先ほど作成した仮想デバイスを選択して、「Start...」をクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-10.png

|

・「Launch」をクリックします。

.. image:: images/qte-study-vol15-lesson1-mac-11.png

|

・Android エミュレーターが起動することを確認します。

.. image:: images/qte-study-vol15-lesson1-mac-12.png

|

(3) テストコードの実行
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

・sample-code 配下の examples/java/junit/pom.xml をテキストエディタで開きます。以下の通りに修正し、保存します。

.. image:: images/qte-study-vol15-lesson1-mac-13.png

|

・ターミナルを起動します。ターミナル起動後、「レッスン0」でダウンロードした sample-code 配下の examples/java/junit まで移動します。

.. image:: images/qte-study-vol15-lesson1-mac-14.png

|

・ターミナルで「mvn -Dtest=com.saucelabs.appium.AndroidWebViewTest test」と入力し、Enterを押します。

.. image:: images/qte-study-vol15-lesson1-mac-15.png

